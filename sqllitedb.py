import os, sys, sqlite3
#https://www.informatik-aktuell.de/betrieb/datenbanken/datenbanken-mit-python-und-sqlite.html
#1968808315:AAE_n_62WAAmngCfb1E6Xb0oRVIfiHtSPCI //family tg bot token
# Existenz feststellen
if os.path.exists("geburtstage_test.db"):
    print("Datei bereits vorhanden")
    sys.exit(0)
else:
    # Verbindung zur Datenbank erzeugen
    connection = sqlite3.connect("geburtstage_test.db")

    # Datensatz-Cursor erzeugen
    cursor = connection.cursor()

    # Datenbanktabelle erzeugen
    sql = "CREATE TABLE personen(" \
          "vorname TEXT, " \
          "nachname TEXT, " \
          "geburtstag TEXT)"
    cursor.execute(sql)
    #DD.MM.YYY

    # Datensatz erzeugen
    sql = "INSERT INTO personen VALUES('Calvin', " \
          "'Hübel', '09.03.1994')"
    cursor.execute(sql)
    connection.commit()

    # Datensatz erzeugen
    sql = "INSERT INTO personen VALUES('Nikita', " \
          "'Sia Shu-Wen', '22.11.1989')"
    cursor.execute(sql)
    connection.commit()

    # Verbindung beenden
    connection.close()

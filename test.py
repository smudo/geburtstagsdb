#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=C0116,W0613

import sqlite3
import os
import datetime
import logging
import telegram

from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

#globe vars
my_token = '1968808315:AAE_n_62WAAmngCfb1E6Xb0oRVIfiHtSPCI'
filename = "user_liste.txt"
globchatid = ""
 # time when send the birthday reminder
hour = 11  # -1
minute = 0


# send telegram message

def send(msg, chat_id, token=my_token):
    """
	Send a mensage to a telegram user specified on chatId
	chat_id must be a number!
	"""
    bot = telegram.Bot(token=token)
    bot.sendMessage(chat_id=chat_id, text=msg)


# send alarm

def alarm(context, chat_id, gebvorname, gebnachname, gebdatum):
    """Send the alarm message."""
    #print("conteeext jobid:", jobid)
    #job = context.job
    #print("conteeext jobid:", jobid)
    alarmtext = 'jemand hat geburtstag!!!: ' + gebvorname + gebnachname + gebdatum
    #context.bot.send_message(chat_id=chat_id, text=alarmtext)
    context.bot.send_message(chat_id, text=alarmtext)

def alarmzwei(context: CallbackContext) -> None:
    """Send the alarm message."""
    job = context.job
    # context.bot.send_message(job.context, text='Beep!')
    chatid = readfile("temp")
    print("read chat id from temp", chatid)
    print("job::", job)
    print("job name:", job.name)
    print("job context hier:", job.context)
    jobname = job.name.split("gebjob:")
    print("jobname[1]:", jobname[1])
    print("jobname[0]:", jobname[0])
    context.bot.send_message(job.context, text=jobname[1])


# Datenbank eingabe

def insertgb(vorname, nachname, gebdatum):
    if os.path.exists("geburtstage_test.db"):
        connection = sqlite3.connect("geburtstage_test.db")

        print("db vorhanden")

        # Datensatz-Cursor erzeugen
        cursor = connection.cursor()

        # Datensatz erzeugen
        cursor.execute("INSERT INTO personen VALUES (?,?,?)", (vorname, nachname, gebdatum))
        connection.commit()

        # Verbindung beenden
        connection.close()


# Datenbank Abfrage

def getdbdata():
    geburtstagegesamt = {}

    # Verbindung, Cursor
    connection = sqlite3.connect("geburtstage_test.db")
    cursor = connection.cursor()

    # SQL-Abfrage
    sql = "SELECT rowid, * FROM personen"

    # Absenden der SQL-Abfrage
    cursor.execute(sql)

    print("cursor:", cursor)

    # Ausgabe des Ergebnisses
    n = 0
    for dsatz in cursor:
        print(dsatz[0], dsatz[1], dsatz[2], dsatz[3])
        geburtstagegesamt[n] = [dsatz[1], dsatz[2], dsatz[3], dsatz[0]]
        n += 1

    # geburtstage2 = {'geburtstag': ['Calvin Hübel', '09.03.1994'], 'geburtstag2': ['Nikita Sia', '19.10.1989']}
    # print(type(geburtstage2))

    print("geburtstagegesamt:", type(geburtstagegesamt))
    print(geburtstagegesamt)
    #{0: ['sdfsdf', 'sdfsdf', '07.07.1983', 6], 1: ['sdfsdf', 'sdfsdfds', '20.12.1960', 7], 2: ['Gar...

    return geburtstagegesamt


# Datenbank Eintrag löschen

def deletegb(index):
    if os.path.exists("geburtstage_test.db"):
        connection = sqlite3.connect("geburtstage_test.db")

        print("db vorhanden")
        print("deleting ID:", index)

        # Datensatz-Cursor erzeugen
        cursor = connection.cursor()

        # Datensatz erzeugen
        sql_update_query = """DELETE from personen where rowid = ?"""
        cursor.execute(sql_update_query, (index,))

        # cursor.execute("DELETE from personen where ID (?)", (index))
        connection.commit()

        # Verbindung beenden
        connection.close()


# Datenbank auf Geburtstag prüfen

def checkgeburtstag():
    print("dbvalues:", getdbdata())
    testdic = {0: ['Calvin', 'Hübel', '09.03.1994'], 1: ['Nikita', 'Sia Shu-Wen', '22.11.1989'],
               2: ['max', 'max', '01.01.1999'], 3: ['max', 'musterman', '01.01.1999'],
               4: ['test', 'malschauen', '02.11.1999']}

    # get todaysday
    x = datetime.datetime.now()
    todaysday = x.strftime("%d.%m")
    print("todaysday:", todaysday)

    # search for todaysday in dbvalues
    values = []
    for value in getdbdata().values():
        print("value[1][0:6]", value[2][0:6])
        if todaysday in value[2][0:6]:
            values.append(value)
    print(type(values))
    txt = "Geburtstag Heute!!! :" + str(values)
    send(txt, 160596794)
    return values


# get Birthdays as datetime format from DB

def createallbirthdayjobs(update, context):
    # geburtstage werden aus DB abgegriffen und mit tg-bot-jobs angelegt
    ids = readfromfile()  # return dic {'Nikita': '1525720456', 'Smudio': '160596794'}
    print("conteeext createallbirthdayjobs:", context)
    data = getdbdata()
    context = context
    chat_id = update.message.chat_id
    # date = datetime.datetime(2021, 11, 24, 23, 58, 00)
    #context.job_queue.run_once(alarmzwei, date, context=chat_id, name=str(chat_id))
    #try:
    for chatidname, chatid in ids.items():
        n = 0
        for n in data:
            print("eintreage:", data)
            print("conteeext:", context)
            job = context.job
            print("conteeext:", job)
            print("dataaa:", data)
            print("nnn:", n)
            print("chatidchatidchatid:", chatid)
            savefile("temp", chatid)
            #date = datetime.datetime(2021, 11, 24, 23, 58, 00)
            #context.job_queue.run_once(alarmzwei, date, context=chatid, name=str(chatid))
            print("oder kommt das noch???")
            registerjobs(data[n][0], data[n][1], data[n][2], chatid, context)
            print("ende???")
        n += 1
        print("ende zwei???")
    #except Exception as e:
     #   print("An exception occurred")
      #  print(e)

        # read ID's from file:
    #ids = readfromfile()  # return dic {'Nikita': '1525720456', 'Smudio': '160596794'}
    #print("++++++++++++++ ids:", ids)
    #for value, key in ids.items():
    #    print("name und chatid der empfänger welche die geburtstagsjobs bekommen:", value, key)
    #    # for every id, every birthday.
    #    ## Todo: get birthdays from db and use there dates
    #    # test remember job
    #    date = datetime.datetime(2021, 11, 16, 15, 11, 00)
    #    ## Todo: and create the jobs
    #    context.job_queue.run_once(alarm, date, context=key, name=str(key))
    #user = update.effective_user
    #update.message.reply_text("Geburtstags erinnerungen platziert")


    #return vorname, nachname, geburtstag



# Telegram Bot
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

def regjob(update, context):
    chat_id = update.message.chat_id
    date = datetime.datetime(2021, 11, 24, 0, 56, 00)
    #context.job_queue.run_once(alarmzwei, date, context=chat_id, name=str(chat_id))


def registerjobs(vorname, nachname, gebtag, chatid, context: CallbackContext) -> None:
    #print("!!!!!!!! Reigster JOB:", vorname, nachname, gebtag, chatid)
    print("vorname:", vorname)
    print("nachname:", nachname)
    print("gebtag:", gebtag)
    print("chatid:", chatid)
    #datetime_object = datetime.datetime.strptime(gebtag, '%d.%m.%y')
    birthsplit = gebtag.split(".")
    print("Birthsplit::", birthsplit)
    # add time to datetime_object
    #print("datetime_object:", datetime_object)
    # chat_id = 160596794 #meine
    # -1 h
    # https://stackoverflow.com/questions/62289341/telegram-bot-api-python-run-daily-method
    # datetime.time(hour=6, minute=27, tzinfo=pytz.timezone('Asia/Kolkata'))
    currentDateTime = datetime.datetime.now()
    date = currentDateTime.date()
    year = date.strftime("%Y")
    date = datetime.datetime(int(year), int(birthsplit[1]), int(birthsplit[0]), hour, minute, 00)
    bddate = datetime.datetime(int(year), int(birthsplit[1]), int(birthsplit[0]), hour, minute, 00)
    birthdadate = datetime.datetime(int(year), int(birthsplit[1]), int(birthsplit[0]), 0, 0, 00)
    print("DAAAATE:", date)
    jobname = "job für: " + str(chatid) + ", gebjob:" + "Geburtstag 🎉 \n" + vorname + " " + nachname + "\n geb Datum: " + gebtag
    testjob = context.job
    print("testjobtestjob:", testjob)
    savefile("temp", chatid)
    currentDateTime = datetime.datetime.now()
    date = currentDateTime.date()
    nowyear = date.strftime("%Y")
    nowmonth = date.strftime("%m")
    nowday = date.strftime("%d")
    nowhour = date.strftime("%H")
    nowminute = date.strftime("%M")
    #gebtagdatum = datetime.datetime.strptime(gebtag, '%d.%m.%y')
    now = datetime.datetime.now()
    currentDateTime = now.strftime("%d.%m.%Y")
    currentDateTime = datetime.datetime.strptime(currentDateTime, '%d.%m.%Y')
    newdatenow = str(currentDateTime.day) + "." + str(currentDateTime.month) + "." + str(year)
    gebtagdatum = datetime.datetime.strptime(newdatenow, '%d.%m.%Y')
    print("currentDateTime:", currentDateTime)
    print("ja heute ist ein geburtstag")
    print("gebtagdatum:", gebtagdatum)
    print("currentDateTime:", currentDateTime)
    print("nowminute:", nowminute)
    print("nowminute typ:", type(nowminute))
    print("birthdadate:", birthdadate)
    print("birthdadate type:", type(birthdadate))
    if birthdadate == currentDateTime:
        print("ja heute ist ein geburtstag")
        print("gebtagdatum:", gebtagdatum)
        print("currentDateTime:", currentDateTime)
        print("nowminute:", nowminute)
        print("nowminute typ:", type(nowminute))
        nowminuteplusone = int(now.minute)
        nowminuteplusone += 1
        nowhourminusone = int(now.hour)
        if nowhourminusone == 0:
            nowhourminusone = 24
        nowhourminusone -= 1
        print("year:", year)
        print("year type:", type(year))
        print("gebtagdatum.month:", gebtagdatum.month)
        print("gebtagdatum.month type:", type(gebtagdatum.month))
        print("gebtagdatum.day:", gebtagdatum.day)
        print("gebtagdatum.day type:", type(gebtagdatum.day))
        print("nowhourminusone:", nowhourminusone)
        print("nowhourminusone type:", type(nowhourminusone))
        print("nowminuteplusone:", nowminuteplusone)
        print("nowminuteplusone type:", type(nowminuteplusone))
        newdate = datetime.datetime(int(year), int(gebtagdatum.month), gebtagdatum.day, nowhourminusone, nowminuteplusone, 0)
        print("newdate:.:", newdate)
        # than take hour and minute and change the reminder time to the + 1 minute.
        context.job_queue.run_once(alarmzwei, newdate, context=chatid, name=str(jobname))
    #context.job_queue.run_once(alarm(context, chatid, vorname, nachname, gebtag), date, context=chatid, name=str(jobname))
    print("gebtag::::", bddate)
    context.job_queue.run_once(alarmzwei, bddate, context=chatid, name=str(jobname))
    #update.message.reply_text("Geburtstags erinnerungen platziert")


# Define a few command handlers. These usually take the two arguments update and
def start(update: Update, context) -> None:
    """Send a message when the command /start is issued."""
    regjob(update, context)
    # ------------ start add jobs --------------
    def checkname():
        if update.message.from_user.username == None:
            return update.message.from_user.first_name
        else:
            return update.message.from_user.username
        ## Todo: add groupchatnames --> not possible lol

    # jede ID des users der /start schreibt wird in der temp datei gespeichert
    savetofile(checkname(), update.message.chat_id)
    createallbirthdayjobs(update, context)



def getall(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /geburtstage is issued."""
    gebdata = getdbdata()
    textgeb = ""
    for key in gebdata:
        day = str(gebdata[key][2][0]) + str(gebdata[key][2][1])
        month = str(gebdata[key][2][3]) + str(gebdata[key][2][4])
        year = str(gebdata[key][2][6]) + str(gebdata[key][2][7]) + str(gebdata[key][2][8]) + str(gebdata[key][2][9])
        firststage = ""
        firststage = int(day) + int(month) + int(year)
        firststage = str(firststage)
        secondstage = int(firststage[0]) + int(firststage[1]) + int(firststage[2]) + int(firststage[3])
        chinesnmbr = secondstage
        if int(secondstage) > 9:
            secondstage = str(secondstage)
            chinesnmbr = int(secondstage[0]) + int(secondstage[1])
        print(key, gebdata[key][0], gebdata[key][1], gebdata[key][2])
        textgeb += str(gebdata[key][0])
        textgeb += " "
        textgeb += str(gebdata[key][1])
        textgeb += ": "
        textgeb += str(gebdata[key][2])
        textgeb += ", "
        textgeb += "["
        textgeb += str(chinesnmbr)
        textgeb += "]"
        textgeb += str("\n")
    update.message.reply_text(textgeb)
    # user = update.effective_user
    # update.message.reply_markdown_v2(
    #     fr'{dbdata}',
    #     reply_markup=ForceReply(selective=True),
    # )


def addgeburtstag(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /addgeburtstag is issued."""
    msg = update.message.text
    msgobj = msg.split()
    print("msg:", msg)
    print("msgobj:", msgobj)
    print("msgobj[1]:", msgobj[1])
    print("str(msgobj[1])", str(msgobj[1]))
    vorname = str(msgobj[1])
    nachname = str(msgobj[2])
    geburtsdatum = str(msgobj[3])
    # print("typ(str(msgobj[1]))", str(msgobj[1]))
    print("message text obj:", msgobj[1], msgobj[2], msgobj[3])
    insertgb(vorname, nachname, geburtsdatum)
    replytext = "add", msgobj[1], msgobj[2], msgobj[3]
    update.message.reply_text(replytext)


def deletegeburtstag(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /deletegeburtstag is issued."""
    msg = update.message.text
    msgobj = msg.split()
    print("msg:", msg)
    print("msgobj:", msgobj)
    print("msgobj[1]:", msgobj[1])
    print("str(msgobj[1])", str(msgobj[1]))
    vorname = str(msgobj[1])
    nachname = str(msgobj[2])

    # check for vorname and nachname mit db data and get id
    data = getdbdata()
    n = 0
    for n in data:
        print("vorname:", data[n][0])
        print("nachname:", data[n][1])
        print("rowid:", data[n][3])
        print("n:", n)
        if vorname == data[n][0] and nachname == data[n][1]:
            deletegb(data[n][3])
    n += 1

    # deletegb(vorname, nachname)
    # print("typ(str(msgobj[1]))", str(msgobj[1]))
    print("message text obj:", msgobj[1], msgobj[2])
    replytext = "delete:", msgobj[1], msgobj[2]

    update.message.reply_text(replytext)


def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text('/getall list all birthdays \n/addgeburtstag vorname nachname TT.MM.YYYY \n/delete vorname nachname \n/start set the reminder for all birthdays')


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    update.message.reply_text(update.message.text)


def savefile(filename, content):
    print("save to file:", content)
    file1 = open(filename, "w")
    file1.write(content)
    file1.close()


def readfile(filename):
    f = open(filename, 'r')
    if f.mode == 'r':
        contents = f.read()
        print("read from file:", contents)

    return contents


def savetofile(name, chat_id):
    chat_id_str = str(chat_id)
    f = open(filename, "r+")
    lines = f.readlines()
    filecontent = {}
    idvorhanden = False
    listeannamen = []
    namedoppelt = False
    for line in lines:
        linesplit = line.split("=")
        filecontent[linesplit[0]] = linesplit[1].replace("\n", "")
        # check if Chat_id vorhanden ist. else: nix
        for key, value in filecontent.items():
            listeannamen.append(key)
            if value == chat_id_str:
                print("chat_id vorhanden, kein speichern erfoderlich")
                print("chat_id:", value)
                idvorhanden = True
        # check if name vorhanden ist. else: + suffix
        for keynew, valuenew in filecontent.items():
            if name in listeannamen:
                print("listeannamen:", listeannamen)
                print("name schon vorhanden. füge eintrag mit datum hinzu")
                now = datetime.datetime.now()
                zeitpunkt = now.strftime("%m/%d/%Y, %H:%M:%S")
                keynew = keynew + "@" + zeitpunkt
                print("keynew:", name)
                namedoppelt = True
    if namedoppelt is True:
        name = keynew

    if idvorhanden is False:
        print("save new record:",name , chat_id)
        str_dictionary = repr(chat_id)
        f.write(name + "=" + str_dictionary + "\n")
        f.close()
    else:
        f.close()


def readfromfile():
    f = open(filename, 'r')
    lines = f.readlines()
    filecontent = {}
    for line in lines:
        print("from file: ", line)
        linesplit = line.split("=")
        filecontent[linesplit[0]] = linesplit[1].replace("\n", "")
    print("file content:", filecontent)
    print("typ of file content:", type(filecontent))
    return filecontent  # dict
    ## Todo: return chatid´s


def handlemessage(update, context):
    text = str(update.message.text).lower()
    print("msg erhalten:", text)
    #registerjobs(update, context)
    # response = R.sample_responses(text)
    # update.message.reply_text(response)


def main() -> None:
    # checkgeburtstag()
    # if values are not empty than send this message to all subscribers and all invited groups.

    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater("1968808315:AAE_n_62WAAmngCfb1E6Xb0oRVIfiHtSPCI")

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("getall", getall))
    dispatcher.add_handler(CommandHandler("addgeburtstag", addgeburtstag))
    dispatcher.add_handler(CommandHandler("delete", deletegeburtstag))
    dispatcher.add_handler(MessageHandler(Filters.text, handlemessage))

    # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(
        Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()

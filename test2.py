#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=C0116,W0613
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import sqlite3
import datetime
import logging

from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

# Datenbank Abfrage
def getdbdata():
    geburtstagegesamt = {}

    # Verbindung, Cursor
    connection = sqlite3.connect("geburtstage_test.db")
    cursor = connection.cursor()

    # SQL-Abfrage
    sql = "SELECT * FROM personen"

    # Absenden der SQL-Abfrage
    cursor.execute(sql)

    # Ausgabe des Ergebnisses
    n = 0
    for dsatz in cursor:
        print(dsatz[0], dsatz[1], dsatz[2])
        # geburtstagegesamt.append(dsatz[0])
        # geburtstagegesamt.append(dsatz[1])
        # geburtstagegesamt.append(dsatz[2])
        # geburtstage3[n].append(geburtstagegesamt)
        geburtstagegesamt[n] = [dsatz[0], dsatz[1], dsatz[2]]
        n += 1



    geburtstage2 = {'geburtstag': ['Calvin Hübel', '09.03.1994'], 'geburtstag2': ['Nikita Sia', '19.10.1989']}
    print(type(geburtstage2))

    print("geburtstagegesamt:", geburtstagegesamt)
    print(type(geburtstagegesamt))

    return geburtstagegesamt

alldbdata = getdbdata()
dbdata = alldbdata[0], alldbdata[1]
aufbereitetedbdata = ', '.join(map(str, dbdata))
print("datatyp dbdata:", type(dbdata))

# Telegram Bot
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\!',
        reply_markup=ForceReply(selective=True),
    )

def showgeburtstage(update: Update, context: CallbackContext) -> None:
	"""Send a message when the command /geburtstage is issued."""
	update.message.reply_text(dbdata)
    # user = update.effective_user
    # update.message.reply_markdown_v2(
    #     fr'{dbdata}',
    #     reply_markup=ForceReply(selective=True),
    # )

def addgeburtstag(update: Update, context: CallbackContext) -> None:
	"""Send a message when the command /addgeburtstag is issued."""
	update.message.reply_text('schreibe "vorname, nachname, DD.MM.YYYY"')


def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    update.message.reply_text(update.message.text)


def main() -> None:
    # TODO: DB connection

    # Verbindung, Cursor
    connection = sqlite3.connect("geburtstage_test.db")
    cursor = connection.cursor()

    # SQL-Abfrage
    sql = "SELECT * FROM personen"

    # Kontrollausgabe der SQL-Abfrage
    # print(sql)

    # Absenden der SQL-Abfrage
    cursor.execute(sql)
    dbvalues = []
    # Ausgabe des Ergebnisses
    for dsatz in cursor:
        print(dsatz[0], dsatz[1], dsatz[2])
        dbvalues.append([dsatz[0], dsatz[1], dsatz[2]])

    # Verbindung beenden
    connection.close()
    print("dbvalues:", dbvalues)
    geburtstage2 = {'geburtstag': ['Calvin Hübel', '09.03.1994'], 'geburtstag2': ['Nikita Sia', '19.10.1989']}
                        #['Calvin', 'Hübel', '09.03.1994'], ['Nikita', 'Sia Shu-Wen', '22.11.1989']
    geburtstag2 = getdbdata()

    # get date
    x = datetime.datetime.now()
    todaysday = x.strftime("%d.%m")
    print("todaysday:", todaysday)

    # search for todaysday in dbvalues
    values = []
    fakedbvalues = geburtstage2
    for value in geburtstage2.values():
        if todaysday in value[1][0:6]:
            value.append(value)
    print(type(values))
    # if values are not empty than send this message to all subscribers and all invited groups.

    # if birthday == today
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater("1968808315:AAE_n_62WAAmngCfb1E6Xb0oRVIfiHtSPCI")

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("showgeburtstage", showgeburtstage))
    dispatcher.add_handler(CommandHandler("addgeburtstag", addgeburtstag))

    # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()






if __name__ == '__main__':
    main()
